# tp-tools
downloads files from TopProver and submits solutions to TopProver

*Currently, tp-tools only supports Coq. Lean is not supported.*

## Usage
- To download .v files, run `tp-tools dl problem_id` (replace `problem_id` with the problem id you attempt to prove)
- To submit your solution, run `tp-tools submit problem_id file` (replace `file` with the path)
